#!/usr/bin/python -d
import binascii
import struct

output = ""

byte_mark = 0
bit_mark = 0 
current_byte_val= 0
byte_fresh = False

image_file = open("Image.113",'rb')

def four_bits():
  bit_0 = read_bit()
  bit_1 = read_bit()
  bit_2 = read_bit()
  bit_3 = read_bit()
  if ((bit_0 == 1) and (bit_1 == 1) and (bit_2 == 1) and (bit_3 == 1)):
    return 15 + four_bits()
  elif ((bit_0 == 0) and (bit_1 == 0) and (bit_2 == 0) and (bit_3 == 0)):
    return 0
  elif ((bit_0 == 0) and (bit_1 == 0) and (bit_2 == 0) and (bit_3 == 1)):
    return 1
  elif ((bit_0 == 0) and (bit_1 == 0) and (bit_2 == 1) and (bit_3 == 0)):
    return 2 
  elif ((bit_0 == 0) and (bit_1 == 0) and (bit_2 == 1) and (bit_3 == 1)):
    return 3
  elif ((bit_0 == 0) and (bit_1 == 1) and (bit_2 == 0) and (bit_3 == 0)):
    return 4 
  elif ((bit_0 == 0) and (bit_1 == 1) and (bit_2 == 0) and (bit_3 == 1)):
    return 5
  elif ((bit_0 == 0) and (bit_1 == 1) and (bit_2 == 1) and (bit_3 == 0)):
    return 6 
  elif ((bit_0 == 0) and (bit_1 == 1) and (bit_2 == 1) and (bit_3 == 1)):
    return 7
  elif ((bit_0 == 1) and (bit_1 == 0) and (bit_2 == 0) and (bit_3 == 0)):
    return 8
  elif ((bit_0 == 1) and (bit_1 == 0) and (bit_2 == 0) and (bit_3 == 1)):
    return 9
  elif ((bit_0 == 1) and (bit_1 == 0) and (bit_2 == 1) and (bit_3 == 0)):
    return 10
  elif ((bit_0 == 1) and (bit_1 == 0) and (bit_2 == 1) and (bit_3 == 1)):
    return 11
  elif ((bit_0 == 1) and (bit_1 == 1) and (bit_2 == 0) and (bit_3 == 0)):
    return 12
  elif ((bit_0 == 1) and (bit_1 == 1) and (bit_2 == 0) and (bit_3 == 1)):
    return 13
  elif ((bit_0 == 1) and (bit_1 == 1) and (bit_2 == 1) and (bit_3 == 0)):
    return 14
  else:
    print "shouldn't happen"
    exit()

def parse_length():
  bit_0 = read_bit()
  bit_1 = read_bit()
  if ((bit_0 == 0) and (bit_1 == 0)):
    return 2
  elif ((bit_0 == 0) and (bit_1 == 1)):
    return 3
  elif ((bit_0 == 1) and (bit_1 == 0)):
    return 4
  elif ((bit_0 == 1) and (bit_1 == 1)):
    bit_2 = read_bit()
    bit_3 = read_bit()
    if ((bit_2 == 0) and (bit_3 == 0)):
      return 5
    elif ((bit_2 == 0) and (bit_3 == 1)):
      return 6
    elif ((bit_2 == 1) and (bit_3 == 0)):
      return 7
    elif ((bit_2 == 1) and (bit_3 == 1)):
      return 8 + four_bits()
    else:
      print "shouldn't happen"
      exit()

def read_byte():
    byte = 0
    mask = int('10000000',2)
    for i in range(8):
        this_mask = mask
        #print "mask is" + str(mask)
        if (read_bit()):
            this_mask = this_mask & int('11111111',2)
        else:
            this_mask = int('00000000',2)
        #print "this_mask is" + str(this_mask)
        byte = byte | this_mask
        #print "byte is" + str(byte)
        mask = mask >> 1
    return byte


def read_bit():
    global image_file
    global byte_mark
    global bit_mark
    global current_byte_val
    global byte_fresh
    if (not byte_fresh):
        str_in = image_file.read(1)
        #print binascii.hexlify(str_in)
        current_byte_val = struct.unpack('b',str_in)[0]
        byte_fresh = True
    mask = int('00000001',2) << (7-bit_mark)
    bit_val = current_byte_val & mask
    #print "bit val is " + str(bit_val)
    bit_mark = bit_mark + 1
    if (bit_mark == 8):
        bit_mark = 0
        byte_mark = byte_mark + 1
        byte_fresh = False
    if (bit_val == 0):
        #print "0"
        return 0
    else:
        #print "1"
        return 1

expanded = ""
image_file.seek(0x7cec);
for i in range(30):
    for j in range(8):
        print read_bit()
    print 'xx'
print 'xxx'
for i in range(50):
    print read_bit()


