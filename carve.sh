#!/bin/bash

fn=dec.Image.113
loc=0
sz=`stat -f%z $fn`
while [ $loc -lt $sz ]
do
    magic=`./searchbin.py -m 1 -p CC33CC33 -s $loc $fn | sed -n '1 p' | sed 's/.*:\ *//' | tr -s ' ' | cut -d ' ' -f 1`
    #echo $magic
    #end=`./searchbin.py -m 1 -p 99669966 -s $magic $fn | sed -n '1 p' | sed 's/.*:\ *//' | tr -s ' ' | cut -d ' ' -f 1`
    #echo $end
    next_magic=`./searchbin.py -m 1 -p CC33CC33 -s $[$magic+1] $fn | sed -n '1 p' | sed 's/.*:\ *//' | tr -s ' ' | cut -d ' ' -f 1`
    #echo $next_magic
    next_end=`./searchbin.py -m 1 -p 996699660700 -s $magic $fn | sed -n '1 p' | sed 's/.*:\ *//' | tr -s ' ' | cut -d ' ' -f 1`
    #echo $next_end
    ./carve.py $magic $next_magic $next_end
    loc=$[$magic+1]
done


