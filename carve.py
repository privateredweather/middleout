#!/usr/bin/python -d
import binascii
import struct
import sys
import os
import datetime

offset=int(sys.argv[1])
next_offset=int(sys.argv[2])
next_end=int(sys.argv[3])
file_handle = open("dec.Image.113","rb")
file_handle.seek(offset)
#magic = struct.unpack('BBBB',file_handle.read(4))
magic = file_handle.read(4)
#print binascii.hexlify(magic)
# timestamps can be grabbed here
#file_handle.seek(0x45,1)
file_handle.seek(0x2D,1)
creation_time = struct.unpack('<I',file_handle.read(4))[0]
creation_time = str(datetime.datetime.fromtimestamp(creation_time))
file_handle.seek(4,1)
access_time = struct.unpack('<I',file_handle.read(4))[0]
access_time = str(datetime.datetime.fromtimestamp(access_time))
file_handle.seek(4,1)
modified_time = struct.unpack('<I',file_handle.read(4))[0]
modified_time = str(datetime.datetime.fromtimestamp(modified_time))
file_handle.seek(4,1)
nm_sz = struct.unpack('<H',file_handle.read(2))[0]
#print nm_sz
long_filename = file_handle.read(nm_sz)
long_filename = long_filename.decode('utf-16le')
#print long_filename
file_handle.seek(21,1)
nm_sz = struct.unpack('<H',file_handle.read(2))[0]
#print nm_sz
short_filename = file_handle.read(nm_sz)
#print short_filename
term = file_handle.read(2)
#check for 0a00?
next_match = next_end
if (next_offset < next_end):
    next_match = next_offset
path_length = next_match - file_handle.tell()
full_name = long_filename
if (path_length > 0):
    #print path_length,
    path = file_handle.read(path_length)
    path = path.decode('utf-16le')
    path_list = path.split('\x00')
    path = "/".join(path_list)
    #print " path: " + path
    full_name = path + "/" + long_filename
full_name = str(os.getcwd() + "/OUTPUT/" + full_name)
#print ":".join("{:02x}".format(ord(c)) for c in full_name)
print full_name + ',' + creation_time + ',' + access_time + ',' + modified_time + ',' + str(next_offset - file_handle.tell())
if (next_end > next_offset):
    # probably a directory - create
    #print "directory"
    if (file_handle.tell() == next_offset):
        # we're pointing to CC33CC33
        # so it's time for a directory?
        if not os.path.exists(full_name):
            #print "MAKING"
            os.makedirs(full_name)
        #print "confirmed - directory"
    else:
        print "something is borked"
elif (next_end < next_offset):
    # probably a file - carve
    #print "file"
    if (file_handle.tell() == next_end):
        # we're pointing to 996699660700
        # so it's time for a file?
        term_magic = file_handle.read(6)
        # could check, I suppose
        new_file = open(full_name,"w")
        loc = file_handle.tell()
        while (loc < next_offset):
            byteme = file_handle.read(1)
            #print byteme,
            new_file.write(byteme)
            loc = loc + 1
        new_file.close()
        #print "confirmed - file"
    else:
        print "something is borked"
else:
    print "impossible"
    exit(-1)
exit(0)

#while ((file_handle.tell() < next_end) and (file_handle.tell() < next_offset)):
    #nm_sz = struct.unpack('<H',file_handle.read(2))[0]
    #directory = unicode(file_handle.read(nm_sz))
    #print str(depth) + directory

