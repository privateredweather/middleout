#!/usr/bin/python -d
import binascii
import struct

def open_bitwise(filename):
  return {'file':open(filename,'rb'),'byte_mark':0,'bit_mark':0,'current_byte_val':0,'byte_fresh':False}

def seek_bitwise(bit_handle,location):
  bit_handle['file'].seek(location)
  bit_handle['byte_mark'] = location
  bit_handle['bit_mark'] = 0
  bit_handle['current_byte_val'] = 0
  bit_handle['byte_fresh'] = False

def open_output(filename):
  return {'output':open(filename,'wab'),'log':open(filename + '.log','wa')}

def hex_print(buf):
  print buf
  for i in range(len(buf)):
    print binascii.hexlify(buf[i]) + " ",
  print ""

def read_bit(bit_handle):
  if (not bit_handle['byte_fresh']):
    str_in = bit_handle['file'].read(1)
    bit_handle['current_byte_val'] = struct.unpack('B',str_in)[0]
    bit_handle['byte_fresh'] = True
  mask = int('00000001',2) << (7 - bit_handle['bit_mark'])
  bit_val = bit_handle['current_byte_val'] & mask
  bit_handle['bit_mark'] = bit_handle['bit_mark'] + 1
  if (bit_handle['bit_mark'] == 8):
    bit_handle['bit_mark'] = 0
    bit_handle['byte_mark'] = bit_handle['byte_mark'] + 1
    bit_handle['byte_fresh'] = False
  if (bit_val == 0):
    return 0
  else:
    return 1

def read_two_bits(bit_handle):
  two_bits = 0
  if (read_bit(bit_handle)):
    two_bits = two_bits | int('00000010', 2)
  if (read_bit(bit_handle)):
    two_bits = two_bits | int('00000001', 2)
  return two_bits

def read_four_bits(bit_handle):
  nibble = 0
  if (read_bit(bit_handle)):
    nibble = nibble | int('00001000', 2)
  if (read_bit(bit_handle)):
    nibble = nibble | int('00000100', 2)
  if (read_bit(bit_handle)):
    nibble = nibble | int('00000010', 2)
  if (read_bit(bit_handle)):
    nibble = nibble | int('00000001', 2)
  return nibble

def read_byte(bit_handle):
  byte = 0
  mask = int('10000000', 2)
  for i in range(8):
    this_mask = mask
    if (read_bit(bit_handle)):
      this_mask = this_mask & int('11111111', 2)
    else:
      this_mask = int('00000000', 2)
    byte = byte + this_mask
    mask = mask >> 1
  return byte

def read_short_offset(bit_handle):
  offset = 0
  mask = int('01000000', 2)
  for i in range(7):
    this_mask = mask
    if (read_bit(bit_handle)):
      this_mask = this_mask & int('11111111', 2)
    else:
      this_mask = int('00000000', 2)
    offset = offset + this_mask
    mask = mask >> 1
  return offset

def read_long_offset(bit_handle):
  offset = 0
  mask = int('010000000000', 2)
  for i in range(11):
    this_mask = mask
    if (read_bit(bit_handle)):
      this_mask = this_mask & int('111111111111', 2)
    else:
      this_mask = int('000000000000', 2)
    offset = offset + this_mask
    mask = mask >> 1
  return offset

def read_length(bit_handle):
  length = read_two_bits(bit_handle) + 2
  if length < 5:
    return length
  else:
    length = length + read_two_bits(bit_handle)
    if length < 8:
      return length
    else:
      looping = True
      while (looping):
        four_bits = read_four_bits(bit_handle)
        if (four_bits < 15):
          looping = False
          length = length + four_bits
          return length
        length = length + four_bits

def compressed(bit_handle):
  return read_bit(bit_handle)

def short_offset(bit_handle):
  return read_bit(bit_handle)

def parse(bit_handle, term_byte, out_handle):
  decompressed = ""
  #for i in range(1000):
   # decompressed = decompressed + " "
  #current_location = 1000
  current_location = 0
  original_mark = bit_handle['byte_mark']
  while (bit_handle['byte_mark'] < original_mark + term_byte):
    length = 0
    offset = 0
    if (compressed(bit_handle)):
      if (short_offset(bit_handle)):
        offset = read_short_offset(bit_handle)
        if (offset > 127):
          print "code bug - short offset can't be bigger than 127"
          exit(0)
        #print "offset is" + str(offset)
        if (offset == 0):
          print "valid til end marker byte " + str(bit_handle['byte_mark'])
          #print decompressed[1000:] + "::" + binascii.hexlify(decompressed[1000:])
          print "size of decompressed is " + str(current_location)
          out_handle['output'].write(decompressed)
          log_entry = "Decompressed bytes@ " + str(hex(original_mark)) + " after-size= " + str(current_location) + " end location= " + str(hex(original_mark + current_location)) +  "\n"
          out_handle['log'].write(log_entry)
          #hex_print(decompressed)
          return True
      else:
        offset = read_long_offset(bit_handle)
        if (offset > 2047):
          print "code bug - long offset can't be bigger than 2047"
          exit(0)
        #print "offset is" + str(offset)
      length = read_length(bit_handle)
      if (length < 2):
        print "length is " + str(length)
        print "alignment incorrect - not a valid length - returning"
        log_entry = "alignment incorrect@ " + str(hex(original_mark))
        out_handle['log'].write(log_entry)
        return False
      #print "length is" + str(length)
      while (length > 0):
        try:
          decompressed = decompressed + decompressed[current_location - offset]
        except:
          print "misaligned - current location " + str(current_location) + " offset " + str(offset)
          log_entry = "misaligned@ " + str(hex(original_mark)) + ", " + str(current_location)
          out_handle['log'].write(log_entry)
          return -1
        current_location = current_location + 1
        length = length - 1
    else:
      #decompressed = decompressed + str(read_byte(bit_handle))
      decompressed = decompressed + struct.pack('B',read_byte(bit_handle))
      current_location = current_location + 1
  print "valid til term byte"
  #print decompressed[1000:] + "::" + binascii.hexlify(decompressed[1000:])
  #hex_print(decompressed)
  log_entry = "NO END MARK Decompressed bytes@ " + str(hex(original_mark)) + " expanded to " + str(hex(current_location)) + ", " + str(current_location) + "\n"
  out_handle['log'].write(log_entry)
  return True

def try_to_parse(filename, starting_byte, ending_byte):
  current_byte = starting_byte
  while (current_byte < ending_byte):
    bit_handle = open_bitwise(filename)
    for i in range(current_byte):
      read_byte(bit_handle)
    print"++++++++++++++++++++++"
    print "starting at byte " + str(current_byte) + ":"
    #parse(bit_handle, 0x7500)
    #parse(bit_handle, 0x7500)
    current_byte = current_byte + 1


#try_to_parse("Image.113",0x15d00,0x15d01)
#try_to_parse("Image.113",0x8290a,0x8290b)
#try_to_parse("sample2",0,5)
#try_to_parse("sample",0,20)

#for i in range(5):
i = 1
bit_handle = open_bitwise("Image.113")
out_handle = open_output("dec.Image.113")
while True:
  locale = 0x010a + (i * 0x7400)
  seek_bitwise(bit_handle, locale)
  parse(bit_handle, 0x7500, out_handle)
  i = i + 1
  out_handle['log'].flush()

#print "locale is " + str(locale)
#parse(bit_handle, 0x7500, out_handle)
  

  


